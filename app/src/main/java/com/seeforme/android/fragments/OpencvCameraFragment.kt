package com.seeforme.android.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import com.example.android_opencv.R
import com.seeforme.android.MainActivity
import com.seeforme.android.Person
import com.seeforme.android.ai.FaceRecognition
import com.seeforme.android.ai.ObjectDetector
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class OpencvCameraFragment : Fragment(), CameraBridgeViewBase.CvCameraViewListener2 {
    private var TAG = "OpencvCamera"

    private lateinit var mRGBA: Mat
    private lateinit var javaCameraView: CameraBridgeViewBase

    private var ID_CAMERA = CameraBridgeViewBase.CAMERA_ID_BACK

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_opencv_camera, container, false)
    }

    override fun onStart() {
        super.onStart()

        javaCameraView = requireActivity().findViewById(R.id.opencv_camera)
        javaCameraView.visibility = SurfaceView.VISIBLE
        javaCameraView.setCvCameraViewListener(this)
    }

    private val mLoaderCallback: BaseLoaderCallback = object : BaseLoaderCallback(context) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                SUCCESS -> {
                    run {
                        Log.i(TAG, "OpenCv Is loaded")
                        javaCameraView.enableView()
                    }
                    run { super.onManagerConnected(status) }
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        mRGBA = Mat()
    }

    override fun onCameraViewStopped() {
        mRGBA.release()
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat {
        mRGBA = inputFrame.rgba()

        // Flip image to get mirror effect
        val orientation = javaCameraView.screenOrientation
        if (javaCameraView.isEmulator)
        // Treat emulators as a special case
            Core.flip(mRGBA, mRGBA, 1) // Flip along y-axis
        else {
            when (orientation) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT, ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT -> {
                    if (ID_CAMERA == CameraBridgeViewBase.CAMERA_ID_BACK) Core.flip(
                        mRGBA,
                        mRGBA,
                        1
                    ) // Flip along x-axis 1
                    Core.flip(mRGBA, mRGBA, 0) // Flip along x-axis 0
                }
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE, ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE -> {
                    if (ID_CAMERA == CameraBridgeViewBase.CAMERA_ID_BACK) Core.flip(
                        mRGBA,
                        mRGBA,
                        0
                    ) // Flip along x-axis 0
                    Core.flip(mRGBA, mRGBA, 1) // Flip along y-axis 1
                }
            }
        }

        Core.rotate(mRGBA, mRGBA, Core.ROTATE_90_COUNTERCLOCKWISE)


        if ((requireActivity() as MainActivity).isFaceRecognition()) {
            (requireActivity() as MainActivity).getFaceRecognition().detectFaces(
                (requireActivity() as MainActivity).getCascadeClassifier(),
                mRGBA,
                (requireActivity() as MainActivity).getPersons(), false
            )
            Core.rotate(mRGBA, mRGBA, Core.ROTATE_90_CLOCKWISE)
        } else {
            Core.rotate(mRGBA, mRGBA, Core.ROTATE_90_CLOCKWISE)
            return (requireActivity() as MainActivity).getObjectDetector()
                .recognizeImage(mRGBA, false)
        }

        return mRGBA
    }

    private var baseLoaderCallback = object : BaseLoaderCallback(context) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {

                    javaCameraView.setCameraIndex(ID_CAMERA)
                    javaCameraView.enableView()
                }
                else ->
                    super.onManagerConnected(status)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        javaCameraView.disableView()
    }

    override fun onPause() {
        super.onPause()
        javaCameraView.disableView()
    }

    override fun onResume() {
        super.onResume()
        if (OpenCVLoader.initDebug()) {
            Log.d(TAG, "OpenCV is Configured or Connected Successfully")
            baseLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        } else {
            Log.d(TAG, "OpenCV is not working or Loaded")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, context, baseLoaderCallback)
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, context, mLoaderCallback)
        }
    }

    companion object {

        fun newInstance(): OpencvCameraFragment {
            return OpencvCameraFragment()
        }
    }
}