package com.seeforme.android.fragments

import android.hardware.usb.UsbDevice
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.Surface
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.android_opencv.R
import com.jiangdg.usbcamera.AlertCustomDialog
import com.jiangdg.usbcamera.DeviceInfo
import com.jiangdg.usbcamera.UVCCameraHelper
import com.jiangdg.usbcamera.UVCCameraHelper.OnMyDevConnectListener
import com.jiangdg.usbcamera.utils.FileUtils
import com.seeforme.android.MainActivity
import com.serenegiant.usb.CameraDialog.CameraDialogParent
import com.serenegiant.usb.USBMonitor
import com.serenegiant.usb.widget.CameraViewInterface
import org.opencv.core.CvType
import org.opencv.core.Mat


class UsbCameraFragment : Fragment(), CameraDialogParent, CameraViewInterface.Callback {
    @Suppress("PrivatePropertyName")
    private val TAG: String = "UsbCameraFragment"

    private lateinit var mTextureView: View

    private var mCameraHelper: UVCCameraHelper? = null
    private var mUVCCameraView: CameraViewInterface? = null

    private var isRequest = false
    private var isPreview = false
    private var openingCamera = false

    private var fragment:Fragment = this
    private lateinit var otherFragment:Fragment;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_usb_camera, container, false)
    }

    override fun onStart() {
        super.onStart()

        mTextureView = requireActivity().findViewById(R.id.camera_view)

        mUVCCameraView = mTextureView as CameraViewInterface
        mUVCCameraView!!.setCallback(this)

        mCameraHelper = UVCCameraHelper.getInstance(640, 480)

        mCameraHelper!!.initUSBMonitor(activity, mUVCCameraView, listener)

        mCameraHelper!!.setOnPreviewFrameListener { previewByteArray ->
            val mat = Mat(mTextureView.measuredWidth, mTextureView.measuredHeight, CvType.CV_8UC3)
            mat.put(0, 0, previewByteArray)

            if ((requireActivity() as MainActivity).isFaceRecognition()) {
                (requireActivity() as MainActivity).getFaceRecognition().detectFaces(
                    (requireActivity() as MainActivity).getCascadeClassifier(),
                    mat,
                    (requireActivity() as MainActivity).getPersons(), true
                )
            } else {
                (requireActivity() as MainActivity).getObjectDetector().recognizeImage(mat, true)
            }
        }

        if (mCameraHelper != null) {
            mCameraHelper!!.registerUSB()
        }

        val tmp: MutableList<String?>? = getResolutionList()
        if (tmp != null && tmp.size >= 2) {
            val width = Integer.valueOf(tmp[0])
            val height = Integer.valueOf(tmp[1])
            mCameraHelper!!.updateResolution(width, height)
        }

        if (!openingCamera) {
            otherFragment = OpencvCameraFragment.newInstance()
            requireActivity().supportFragmentManager
                .beginTransaction()
                .add(R.id.frameLayout, otherFragment)
                .commit()
        }
    }

    override fun onStop() {
        super.onStop()
        // step.3 unregister USB event broadcast
        if (mCameraHelper != null) {
            mCameraHelper!!.unregisterUSB()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        FileUtils.releaseFile()
        // step.4 release uvc camera resources
        if (mCameraHelper != null) {
            mCameraHelper!!.release()
        }
    }

    private fun getResolutionList(): MutableList<String?>? {
        val list = mCameraHelper!!.supportedPreviewSizes
        var resolutions: MutableList<String?>? = null
        if (list != null && list.size != 0) {
            resolutions = java.util.ArrayList()
            for (size in list) {
                if (size != null) {
                    resolutions.add(size.width.toString() + "x" + size.height)
                }
            }
        }
        return resolutions
    }

    private fun showShortMsg(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun getUSBMonitor(): USBMonitor? {
        return mCameraHelper!!.usbMonitor
    }

    override fun onDialogResult(canceled: Boolean) {
        if (canceled) {
            showShortMsg("Annuleren")
        }
    }

    private fun getUSBDevInfo(): List<DeviceInfo>? {
        if (mCameraHelper == null) return null
        val devInfos: MutableList<DeviceInfo> = ArrayList()
        val list = mCameraHelper!!.usbDeviceList
        for (dev in list) {
            val info = DeviceInfo()
            info.pid = dev.vendorId
            info.vid = dev.productId
            devInfos.add(info)
        }
        return devInfos
    }

    private fun popCheckDevDialog() {
        val infoList: List<DeviceInfo>? = getUSBDevInfo()
        if (infoList == null || infoList.isEmpty()) {
            Toast.makeText(context, "Find devices failed.", Toast.LENGTH_SHORT)
                .show()
            return
        }
        val dataList: MutableList<String> = ArrayList()
        for (deviceInfo in infoList) {
            dataList.add(
                "Device：PID_" + deviceInfo.pid
                    .toString() + " & " + "VID_" + deviceInfo.vid
            )
        }
        AlertCustomDialog.createSimpleListDialog(
            context,
            "Please select USB devcie",
            dataList
        ) { postion ->
            mCameraHelper!!.requestPermission(postion)
        }
    }

    private val listener: OnMyDevConnectListener = object : OnMyDevConnectListener {
        override fun onAttachDev(device: UsbDevice) {
            // request open permission
            if (!isRequest) {
                isRequest = true
                popCheckDevDialog()
            }
        }

        override fun onDettachDev(device: UsbDevice) {
            // close camera
            if (isRequest) {
                isRequest = false
                mCameraHelper!!.closeCamera()
                showShortMsg(device.deviceName + " is out")
            }
        }

        override fun onConnectDev(device: UsbDevice, isConnected: Boolean) {
            if (!isConnected) {
                showShortMsg("fail to connect,please check resolution params")
                isPreview = false
            } else {
                isPreview = true
                showShortMsg("connecting")
                // initialize seekbar
                // need to wait UVCCamera initialize over
                Thread {
                    try {
                        Thread.sleep(2500)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    Looper.prepare()
                    Looper.loop()
                }.start()
                var usbCameraFragment = newInstance()
                usbCameraFragment.openingCamera = true;
                requireActivity().supportFragmentManager
                    .beginTransaction()
                    .remove(otherFragment)
                    .replace(R.id.frameLayout, fragment)
                    .commit()
            }
        }

        override fun onDisConnectDev(device: UsbDevice) {
            showShortMsg("disconnecting")
            openingCamera = false
            otherFragment = OpencvCameraFragment.newInstance()
            requireActivity().supportFragmentManager
                .beginTransaction()
                .add(R.id.frameLayout, otherFragment)
                .commit()
        }
    }

    override fun onSurfaceCreated(view: CameraViewInterface?, surface: Surface?) {
        if (!isPreview && mCameraHelper!!.isCameraOpened) {
            mCameraHelper!!.startPreview(mUVCCameraView)
            isPreview = true
        }
    }

    override fun onSurfaceChanged(
        view: CameraViewInterface?,
        surface: Surface?,
        width: Int,
        height: Int
    ) {
    }

    override fun onSurfaceDestroy(view: CameraViewInterface?, surface: Surface?) {
        if (isPreview && mCameraHelper!!.isCameraOpened) {
            mCameraHelper!!.stopPreview()
            isPreview = false
        }
    }

    companion object {

        fun newInstance(): UsbCameraFragment {
            return UsbCameraFragment()
        }
    }
}