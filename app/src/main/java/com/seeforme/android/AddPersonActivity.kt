package com.seeforme.android


import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.*
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.graphics.drawable.toBitmap
import com.example.android_opencv.R
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetector
import com.google.mlkit.vision.face.FaceDetectorOptions
import com.seeforme.android.ai.FaceRecognition
import java.io.IOException

class AddPersonActivity : AppCompatActivity() {
    private var start = true
    private var context: Context = this@AddPersonActivity
    //Output size of model
    private lateinit var facePreview: ImageView
    private lateinit var detector: FaceDetector
    private lateinit var previewInfo: TextView
    private lateinit var chooseImage: ImageButton
    private lateinit var nameText: TextInputEditText
    private lateinit var addPersonButton: Button
    private lateinit var persons: ArrayList<Person>
    private lateinit var faceRecognition: FaceRecognition
    private lateinit var sharedPreferences: SharedPreferences

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_person)

        sharedPreferences = context.getSharedPreferences("savedPersons", Context.MODE_PRIVATE)

        persons = intent.getSerializableExtra("persons") as ArrayList<Person>

        faceRecognition = FaceRecognition(this)
        faceRecognition.initializeModel(assets)

        //Initialize Face Detector
        val highAccuracyOpts = FaceDetectorOptions.Builder()
            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
            .build()
        detector = FaceDetection.getClient(highAccuracyOpts)

        facePreview = findViewById(R.id.imageView2)
        previewInfo = findViewById(R.id.PreviewText)
        chooseImage = findViewById(R.id.imageButton)
        addPersonButton = findViewById(R.id.button)
        nameText = findViewById(R.id.editText)
        chooseImage.setOnClickListener { openDialog() }
        addPersonButton.setOnClickListener {
            if(facePreview.drawable == null){
                Toast.makeText(context, "No image was chosen!", Toast.LENGTH_SHORT).show()
            } else {
                addPerson()
            }
        }
    }

    private fun loadPhoto() {
        start = false
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(
            Intent.createChooser(intent, "Select Picture"),
            SELECT_PICTURE
        )
    }

    private fun Int.getResizedBitmap(bm: Bitmap, newHeight: Int): Bitmap {
        val width = bm.width
        val height = bm.height
        val scaleWidth = toFloat() / width
        val scaleHeight = newHeight.toFloat() / height
        // CREATE A MATRIX FOR THE MANIPULATION
        val matrix = Matrix()
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight)

        // "RECREATE" THE NEW BITMAP
        val resizedBitmap = Bitmap.createBitmap(
            bm, 0, 0, width, height, matrix, false)
        bm.recycle()
        return resizedBitmap
    }

    //Similar Analyzing Procedure
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                val selectedImageUri = data!!.data
                try {
                    val impPhoto = InputImage.fromBitmap(getBitmapFromUri(selectedImageUri), 0)
                    detector(impPhoto, getBitmapFromUri(selectedImageUri))
                    facePreview.setImageBitmap(getBitmapFromUri(selectedImageUri))
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                val bitmap = data?.extras?.get("data") as Bitmap
                try {
                    val impPhoto = InputImage.fromBitmap(bitmap, 0)
                    detector(impPhoto, bitmap)
                    facePreview.setImageBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun detector(impPhoto: InputImage, bitmap: Bitmap){
        detector.process(impPhoto).addOnSuccessListener { faces ->
            if (faces.size != 0) {
                facePreview.visibility = View.VISIBLE
                previewInfo.text = "1.Bring Face in view of Camera.\n\n2.Your Face preview will appear here.\n\n3.Click Add button to save face."
                val face = faces[0]
                println(face)

                //write code to recreate bitmap from source
                //Write code to show bitmap to canvas
                var frameBmp: Bitmap? = null
                try {
                    frameBmp = bitmap
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                val frameBmp1 = false.rotateBitmap(frameBmp, 0, false)

                //face_preview.setImageBitmap(frame_bmp1);
                val boundingBox = RectF(face.boundingBox)
                val croppedFace = getCropBitmapByCPU(frameBmp1, boundingBox)
                val scaled = 112.getResizedBitmap(croppedFace, 112)
                facePreview.setImageBitmap(scaled)
                try {
                    Thread.sleep(100)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }.addOnFailureListener {
            start = true
            Toast.makeText(context, "Failed to add", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addPerson() {
        val image = facePreview.drawable.toBitmap()
        val person = Person(nameText.text.toString(), faceRecognition.getEmbedding(image))
        persons.add(person)
        val editor = sharedPreferences.edit()
        editor.putStringSet("persons", listToSet(persons))
        editor.apply()

        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("persons", persons)
        startActivity(intent)
    }

    private fun listToSet(arrayList: ArrayList<Person>): Set<String> {
        val gson = Gson()
        val personSet = HashSet<String>()
        for (person in arrayList) {
            val personAsString = gson.toJson(person)
            personSet.add(personAsString)
        }
        return personSet
    }

    @Throws(IOException::class)
    private fun getBitmapFromUri(uri: Uri?): Bitmap {
        val parcelFileDescriptor = contentResolver.openFileDescriptor(uri!!, "r")
        val fileDescriptor = parcelFileDescriptor!!.fileDescriptor
        val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
        parcelFileDescriptor.close()
        return image
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun openDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog)
        val cameraButton = dialog.findViewById<Button>(R.id.button2)
        val folderButton = dialog.findViewById<Button>(R.id.button3)

        cameraButton.setOnClickListener { openCamera(); dialog.dismiss() }
        folderButton.setOnClickListener { loadPhoto(); dialog.dismiss() }

        dialog.show()
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, 1888)
    }

    companion object {
        private const val SELECT_PICTURE = 1
        private fun getCropBitmapByCPU(source: Bitmap?, cropRectF: RectF): Bitmap {
            val resultBitmap = Bitmap.createBitmap(cropRectF.width().toInt(),
                cropRectF.height().toInt(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(resultBitmap)

            // draw background
            val paint = Paint(Paint.FILTER_BITMAP_FLAG)
            paint.color = Color.WHITE
            canvas.drawRect( //from  w w  w. ja v  a  2s. c  om
                RectF(0F, 0F, cropRectF.width(), cropRectF.height()),
                paint)
            val matrix = Matrix()
            matrix.postTranslate(-cropRectF.left, -cropRectF.top)
            canvas.drawBitmap(source!!, matrix, paint)
            if (!source.isRecycled) {
                source.recycle()
            }
            return resultBitmap
        }

        private fun Boolean.rotateBitmap(
            bitmap: Bitmap?, rotationDegrees: Int, flipX: Boolean
        ): Bitmap {
            val matrix = Matrix()

            // Rotate the image back to straight.
            matrix.postRotate(rotationDegrees.toFloat())

            // Mirror the image along the X or Y axis.
            matrix.postScale(if (flipX) -1.0f else 1.0f, if (this) -1.0f else 1.0f)
            val rotatedBitmap = Bitmap.createBitmap(bitmap!!, 0, 0, bitmap.width, bitmap.height, matrix, true)

            // Recycle the old bitmap if it has changed.
            if (rotatedBitmap != bitmap) {
                bitmap.recycle()
            }
            return rotatedBitmap
        }
    }
}