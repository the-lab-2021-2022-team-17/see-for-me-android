package com.seeforme.android

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.android_opencv.R
import com.google.gson.Gson
import com.seeforme.android.ai.FaceRecognition
import com.seeforme.android.ai.ObjectDetector
import com.seeforme.android.fragments.UsbCameraFragment
import kotlinx.android.synthetic.main.activity_detection.*
import org.opencv.android.OpenCVLoader
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class MainActivity : AppCompatActivity() {

    private var TAG = "MainActivityOpenCV"

    private val MY_CAMERA_REQUEST_CODE = 100

    private lateinit var modeSwitcher: Button

    private var isFaceRecognition = true

    private lateinit var persons: ArrayList<Person>

    private val REQUIRED_PERMISSION_LIST = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.CAMERA
    )
    private val REQUEST_CODE = 1
    private val mMissPermissions: MutableList<String> = java.util.ArrayList()

    private lateinit var cascadeClassifier: CascadeClassifier
    private lateinit var mCascadeFile: File

    private lateinit var faceRecognition: FaceRecognition
    private lateinit var objectDetector: ObjectDetector
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var soundAlert: SoundAlert

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_detection)
        sharedPreferences = this.getSharedPreferences("savedPersons", Context.MODE_PRIVATE)

        initializePerson()

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), MY_CAMERA_REQUEST_CODE)
        }

        if (isVersionM()) {
            checkAndRequestPermissions()
        }

        if (OpenCVLoader.initDebug()) {
            Log.d(TAG, "OpenCV is Configured or Connected Successfully")
        } else {
            Log.d(TAG, "OpenCV is not working or Loaded")
        }

        btn_register.setOnClickListener {
            val intent = Intent(this, AddPersonActivity::class.java)
            intent.putExtra("persons", persons)
            startActivity(intent)
        }

        soundAlert = SoundAlert(this)

        modeSwitcher = findViewById(R.id.modeSwitch)
        modeSwitcher.setOnClickListener {
            isFaceRecognition = if (isFaceRecognition) {
                soundAlert.speakOut("Mode switched to vehicle detection",TextToSpeech.QUEUE_ADD)
                false
            } else {
                soundAlert.speakOut("Mode switched to face recognition", TextToSpeech.QUEUE_ADD)
                true
            }
        }

        //loading in the different models
        faceRecognition = FaceRecognition(this)
        faceRecognition.initializeModel(assets)

        val myTimer = Timer()
        myTimer.schedule(object : TimerTask() {
            override fun run() {
                faceRecognition.resetDetectedPersons()
            }
        }, 0, 60000)

        objectDetector =
            ObjectDetector(assets, "detect.tflite", "custom_label.txt", 320,
            this)

        try {
            val inputStream =
                resources.openRawResource(R.raw.haarcascade_frontalface_alt2)
            val cascadeDir = getDir("cascade", Context.MODE_PRIVATE)
            mCascadeFile = File(cascadeDir, "haarcascade_frontalface_alt2.xml")
            val os = FileOutputStream(mCascadeFile)

            val buffer = ByteArray(4096)
            var bytesRead: Int?

            do {
                bytesRead = inputStream.read(buffer)
                if (bytesRead == -1) break
                os.write(buffer, 0, bytesRead)
            } while (true)

            inputStream.close()
            os.close()

            cascadeClassifier = CascadeClassifier(mCascadeFile.absolutePath)
            if (cascadeClassifier.empty()) {
                Log.d(TAG, "Failed to load cascade classifier")
            } else
                Log.d(
                    TAG,
                    "Loaded cascade classifier from " + mCascadeFile.absolutePath
                )

            cascadeDir.delete()
        } catch (e: IOException) {
            e.printStackTrace()
            Log.e(TAG, "Failed to load cascade. Exception thrown: $e")
        }

        supportFragmentManager
            .beginTransaction()
            .add(R.id.frameLayout, UsbCameraFragment.newInstance())
            .commit()
    }

    fun getObjectDetector() : ObjectDetector{
        return objectDetector
    }

    fun getFaceRecognition() : FaceRecognition{
        return faceRecognition
    }

    fun getCascadeClassifier() : CascadeClassifier{
        return cascadeClassifier
    }

    fun isFaceRecognition(): Boolean {
        return isFaceRecognition
    }

    private fun initializePerson() {
        val savedPersons = sharedPreferences.getStringSet("persons", null)
        persons = if (savedPersons != null) {
            setToList(savedPersons as HashSet<String>)
        } else ArrayList()
    }

    private fun setToList(hashSet: HashSet<String>): ArrayList<Person> {
        val gson = Gson()
        persons = ArrayList()
        for (person in hashSet) {
            val personAsPerson = gson.fromJson(person, Person::class.java)
            persons.add(personAsPerson)
        }
        return persons
    }

    override fun onResume() {
        super.onResume()
        if (intent.getSerializableExtra("persons") != null) {
            persons = intent.getSerializableExtra("persons") as ArrayList<Person>
            print("Resumed: " + persons.size)
        }
    }

    fun getPersons(): ArrayList<Person> {
        return persons
    }

    private fun isVersionM(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    }

    private fun checkAndRequestPermissions() {
        mMissPermissions.clear()
        for (permission in REQUIRED_PERMISSION_LIST) {
            val result = ContextCompat.checkSelfPermission(this, permission)
            if (result != PackageManager.PERMISSION_GRANTED) {
                mMissPermissions.add(permission)
            }
        }
        // check permissions has granted
        if (mMissPermissions.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                mMissPermissions.toTypedArray<String>(),
                REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE) {
            for (i in grantResults.indices.reversed()) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    mMissPermissions.remove(permissions[i])
                }
            }
        }
        // Get permissions success or not
        if (mMissPermissions.isNotEmpty()) {
            Toast.makeText(
                this,
                "get permissions failed,exiting...",
                Toast.LENGTH_SHORT
            ).show()
            this.finish()
        }
    }

    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }
}