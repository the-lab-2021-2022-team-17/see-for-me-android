package com.seeforme.android.ai

import android.content.Context
import android.content.res.AssetFileDescriptor
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.speech.tts.TextToSpeech
import com.seeforme.android.SoundAlert
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.tracking.MultiTracker
import org.opencv.tracking.TrackerKCF
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.CompatibilityList
import org.tensorflow.lite.gpu.GpuDelegate
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader
import java.lang.Math.pow
import java.lang.reflect.Array
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.channels.FileChannel
import kotlin.math.sqrt


class ObjectDetector internal constructor(
    assetManager: AssetManager,
    modelPath: String,
    labelPath: String,
    private val INPUT_SIZE: Int,
    context: Context
) {
    // should start from small letter
    // this is used to load model and predict
    private val interpreter: Interpreter
    private var soundAlert: SoundAlert = SoundAlert(context)

    // store all label in array
    private val labelList: List<String>
    private val PIXEL_SIZE = 3 // for RGB
    private val IMAGE_MEAN = 0
    private val IMAGE_STD = 255.0f

    // use to initialize gpu in app
    private var height = 0
    private var width = 0

    private var algorith = "KCF"

    private var res = listOf<Any>()
    private var nextId: Int = 0

    private var timeOfLastFrame: Long = 0
    private var timeOfCurrentFrame: Long = 0

    private var prev = mapOf<Int, Trackable>()
    private var current = mapOf<Int, Trackable>()

    private var trackers = MultiTracker.create()

    private var skippedFrames = 0
    private var toSkip = 0


    @Throws(IOException::class)
    private fun loadLabelList(assetManager: AssetManager, labelPath: String): List<String> {
        // to store label
        val labelList: MutableList<String> = ArrayList()
        // create a new reader
        val reader = BufferedReader(InputStreamReader(assetManager.open(labelPath)))
        // loop through each line and store it to labelList
        var line = reader.readLine()
        while (line != null) {
            labelList.add(line)
            line = reader.readLine()
        }
        reader.close()
        return labelList
    }

    @Throws(IOException::class)
    private fun loadModelFile(assetManager: AssetManager, modelPath: String): ByteBuffer {
        // use to get description of file
        val fileDescriptor: AssetFileDescriptor = assetManager.openFd(modelPath)
        val inputStream = FileInputStream(fileDescriptor.getFileDescriptor())
        val fileChannel: FileChannel = inputStream.getChannel()
        val startOffset: Long = fileDescriptor.getStartOffset()
        val declaredLength: Long = fileDescriptor.getDeclaredLength()
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    // create new Mat function
    fun recognizeImage(mat_image: Mat, isUsbCamera: Boolean): Mat {
        timeOfCurrentFrame = System.currentTimeMillis()

        // Rotate original image by 90 degree get get portrait frame

        // This change was done in video: Does Your App Keep Crashing? | Watch This Video For Solution.
        // This will fix crashing problem of the app
        val rotated_mat_image = Mat()
        val a = mat_image.t()
        Core.flip(a, rotated_mat_image, 1)
        // Release mat
        a.release()

        // if you do not do this process you will get improper prediction, less no. of object
        // now convert it to bitmap
        var bitmap: Bitmap? = null
        bitmap = Bitmap.createBitmap(
            rotated_mat_image.cols(),
            rotated_mat_image.rows(),
            Bitmap.Config.ARGB_8888
        )
        Utils.matToBitmap(rotated_mat_image, bitmap)
        // define height and width
        height = bitmap.getHeight()
        width = bitmap.getWidth()

        // scale the bitmap to input size of model
        bitmap = Bitmap.createScaledBitmap(bitmap, INPUT_SIZE, INPUT_SIZE, false)

        var imgData =
            ByteBuffer.allocateDirect(1 * 320 * 320 * 3 * 4);
        imgData.order(ByteOrder.nativeOrder());
        var intValues = IntArray(320 * 320)

        var outputClasses = Array(1) {
            FloatArray(10)
        }

        var outputLocations = Array(1) {
            Array(10) {
                FloatArray(4)
            }
        }
        var outputScores = FloatArray(1)

        var numDetections = Array(1) {
            FloatArray(10)
        }

        bitmap.getPixels(
            intValues,
            0,
            bitmap.getWidth(),
            0,
            0,
            bitmap.getWidth(),
            bitmap.getHeight()
        )

        imgData.rewind()
        for (i in 0 until 320) {
            for (j in 0 until 320) {
                val pixelValue: Int = intValues.get(i * 320 + j)

                imgData.putFloat(((pixelValue shr 16 and 0xFF) - IMAGE_MEAN) / IMAGE_STD)
                imgData.putFloat(((pixelValue shr 8 and 0xFF) - IMAGE_MEAN) / IMAGE_STD)
                imgData.putFloat(((pixelValue and 0xFF) - IMAGE_MEAN) / IMAGE_STD)
            }
        }

        prev = mapOf()
        prev = prev.plus(current)
        current = mapOf()

        var previous = trackers.objects.toList()
        trackers.update(mat_image, trackers.objects)

        val inputArray = arrayOf<Any>(imgData)

        if (skippedFrames >= toSkip) {
            skippedFrames = 0

            val outputMap: MutableMap<Int, Any> = HashMap()
            outputMap[0] = outputClasses
            outputMap[1] = outputLocations
            outputMap[2] = outputScores
            outputMap[3] = numDetections

            // Run the inference call.
            interpreter.runForMultipleInputsOutputs(inputArray, outputMap)


            // now predict
            // Before watching this video please watch my previous 2 video of
            //      1. Loading tensorflow lite model
            //      2. Predicting object
            // In this video we will draw boxes and label it with it's name
            val Object_class = outputMap[3]
            val value = outputMap[1]
            val score = outputMap[0]


            // loop through each object
            // as output has only 10 boxes
            for (i in 0..9) {
                val class_value = Array.get(Array.get(Object_class, 0), i) as Float
                val score_value = Array.get(Array.get(score, 0), i) as Float


                val minimumAccuracy = if (isUsbCamera) {
                    0.085
                } else {
                    0.4
                }

                // Here you can change threshold according to your model
                // Now we will do some change to improve app
                if (score_value > minimumAccuracy) {
                    val box1 = Array.get(Array.get(value, 0), i)
                    // we are multiplying it with Original height and width of frame
                    val top = Array.get(box1, 0) as Float * height
                    val left = Array.get(box1, 1) as Float * width
                    val bottom = Array.get(box1, 2) as Float * height
                    val right = Array.get(box1, 3) as Float * width

                    var close = false
                    var minDist = 1000.0
                    var pre: Map.Entry<Int, Trackable>? = null
                    for (obj in prev) {
                        var dist = sqrt(
                            pow(
                                (top - bottom) - obj.value.center.y,
                                2.0
                            ) + pow((right - left) - obj.value.center.x, 2.0)
                        )
                        if (dist < 5) {
                            if (dist < minDist) {
                                minDist = dist
                                pre = obj
                            }
                            close = true
                        }
                    }
                    if (!close) {
                        register(Point((right - left).toDouble(), (top - bottom).toDouble()))
                        // Makes sure we only track 1 object
                        trackers = MultiTracker.create()
                        trackers.add(
                            TrackerKCF.create(), mat_image,
                            Rect2d(
                                Point(left.toDouble(), top.toDouble()),
                                Point(right.toDouble(), bottom.toDouble())
                            )
                        )
                    } else {
                        current = current.plus(Pair(pre!!.key, pre.value))
                    }


//                    // draw rectangle in Original frame //  starting point    // ending point of box  // color of box       thickness
//                    Imgproc.rectangle(
//                        rotated_mat_image, Point(left.toDouble(), top.toDouble()), Point(
//                            right.toDouble(), bottom.toDouble()
//                        ), Scalar(0.0, 255.0, 0.0, 255.0), 2
//                    )
//                    // write text on frame
//                    // string of class name of object  // starting point                         // color of text           // size of text
//                    Imgproc.putText(
//                        rotated_mat_image, labelList[class_value.toInt()], Point(
//                            left.toDouble(), top.toDouble()
//                        ), 3, 1.0, Scalar(255.0, 0.0, 0.0, 255.0), 2
//                    )
                }
            }
        } else {
            skippedFrames++
        }

        var maxSpeed = 0.0
        var i = 0
        for (tracker in trackers.objects.toList()) {
            val top = tracker.y
            val left = tracker.x
            val bottom = tracker.y + tracker.height
            val right = tracker.x + tracker.width

            Imgproc.rectangle(
                rotated_mat_image, Point(left, top), Point(right, bottom),
                Scalar(0.0, 255.0, 0.0, 255.0), 2
            )

            var speed = 0.0
            if (previous.size > i) {
                speed = calculateSpeed(previous[i].width, tracker.width)
            }
            if (speed > maxSpeed) {
                maxSpeed = speed
            }

            var speedMessage = ""
            when {
                maxSpeed == 0.0 -> speedMessage = "new vehicle detected"
                maxSpeed <= 0.95 -> speedMessage = "driving away"
                maxSpeed <= 1.05 -> speedMessage = "stationary"
                maxSpeed <= 1.15 -> speedMessage = "slow"
                maxSpeed <= 1.4 -> speedMessage = "fast"
                maxSpeed > 1.4 -> speedMessage = "very fast!!!"
                else -> { // Note the block
                    print("error")
                }
            }

            if (speedMessage != "stationary" && speedMessage != "driving away")
                soundAlert.speakOut(speedMessage, TextToSpeech.QUEUE_ADD)

            i++

        }

        val b = rotated_mat_image.t()
        Core.flip(b, mat_image, 0)
        b.release()

        timeOfLastFrame = timeOfCurrentFrame


        // Now for second change go to CameraBridgeViewBase
        return mat_image
    }

    init {
        val compatList = CompatibilityList()

        // use to define gpu or cpu // no. of threads
        val options = Interpreter.Options().apply {
            if (compatList.isDelegateSupportedOnThisDevice) {
                // if the device has a supported GPU, add the GPU delegate
                val delegateOptions = compatList.bestOptionsForThisDevice
                this.addDelegate(GpuDelegate(delegateOptions))
            } else {
                // if the GPU is not supported, run on 4 threads
                this.numThreads = 4
            }
        }
        // loading model
        interpreter = Interpreter(loadModelFile(assetManager, modelPath), options)
        // load labelmap
        labelList = loadLabelList(assetManager, labelPath)
    }

    fun register(center: Point) {
        val id = nextId++
        current = current.plus(Pair(id, Trackable(id, center)))
    }

    fun calculateSpeed(first: Double, second: Double): Double {
        return first / second
    }

    class Trackable(id: Int, center: Point) {
        var id = id
        var center = center
        var lastSeen = 0
    }


}
