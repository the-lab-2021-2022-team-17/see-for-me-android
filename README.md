# See-for-me-Android

# The lab 2021-2022

## Teamname: Team 17

### Team members

| Name | Email | Role |
| ------ | ------ | ------ |
| Jari Moons | jari.moons@student.kdg.be | IFS |
| Thomas Vandewalle | thomas.vandewalle@student.kdg.be | IFS |
| Pieter-Jan Livens | pieterjan.livens@student.kdg.be | IFS |

## Description
This project is an android application.
It has 2 main modes in which it can either detect faces or vehicles.
You can upload pictures of a person in the app which the facial recognition can then later detect.
The vehicle detection recognises cars and bike. It keeps track of them and gives a rough estimate of their speed.
