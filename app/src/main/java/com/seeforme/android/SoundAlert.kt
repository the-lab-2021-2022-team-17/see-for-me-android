package com.seeforme.android

import android.content.Context
import android.speech.tts.TextToSpeech
import android.util.Log
import java.util.*

class SoundAlert internal constructor(
        context: Context
): TextToSpeech.OnInitListener {

    private var tts: TextToSpeech? = TextToSpeech(context, this)

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            val result = tts!!.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_MISSING_DATA) {
                Log.e("TTS", "The language specified is not supported!")
            }
        }
    }

    fun speakOut(text: String, queueType: Int) {
        tts!!.speak(text, queueType, null, "")
    }
}