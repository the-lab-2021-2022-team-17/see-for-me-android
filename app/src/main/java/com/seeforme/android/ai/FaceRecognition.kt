package com.seeforme.android.ai

import android.content.ContentValues.TAG
import android.content.Context
import android.content.res.AssetFileDescriptor
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.speech.tts.TextToSpeech
import android.util.Log
import com.seeforme.android.Person
import com.seeforme.android.SoundAlert
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.objdetect.CascadeClassifier
import org.tensorflow.lite.DataType
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.io.FileInputStream
import java.io.IOException
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import kotlin.math.sqrt

class FaceRecognition internal constructor(context: Context) {
    private lateinit var tfliteModel: MappedByteBuffer
    private lateinit var interpreter: Interpreter
    private lateinit var tImage: TensorImage
    private lateinit var tBuffer: TensorBuffer
    private lateinit var mBitmap: Bitmap
    private var soundAlert = SoundAlert(context)
    private var detectedPersons = ArrayList<String>()

    // Width of the image that our model expects
    private var inputImageWidth = 112

    // Height of the image that our model expects
    private var inputImageHeight = 112

    private val IMAGE_MEAN = 127.5f
    private val IMAGE_STD = 128f

    fun initializeModel(assetManager: AssetManager) {
        try {
            tfliteModel = loadModelFile(assetManager)

            @Suppress("DEPRECATION")
            interpreter = Interpreter(tfliteModel)

            val probabilityTensorIndex = 0
            val probabilityShape =
                interpreter.getOutputTensor(probabilityTensorIndex).shape() // {1, EMBEDDING_SIZE}
            val probabilityDataType = interpreter.getOutputTensor(probabilityTensorIndex).dataType()

            // Creates the input tensor
            tImage = TensorImage(DataType.FLOAT32)

            // Creates the output tensor and its processor
            tBuffer = TensorBuffer.createFixedSize(probabilityShape, probabilityDataType)

            Log.d(TAG, "Model loaded successful")
        } catch (e: IOException) {
            Log.e(TAG, "Error reading model", e)
        }
    }

    private fun loadModelFile(assetManager: AssetManager): MappedByteBuffer {
        val fileDescriptor: AssetFileDescriptor = assetManager.openFd("mobilefacenet.tflite")
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel: FileChannel = inputStream.channel
        val startOffset: Long = fileDescriptor.startOffset
        val declaredLength: Long = fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    private fun loadImage(bitmap: Bitmap): TensorImage {
        // Loads bitmap into a TensorImage
        tImage.load(bitmap)

        val imageProcessor = ImageProcessor.Builder()
            .add(ResizeOp(inputImageHeight, inputImageWidth, ResizeOp.ResizeMethod.BILINEAR))
            .add(NormalizeOp(IMAGE_MEAN, IMAGE_STD))
            .build()
        return imageProcessor.process(tImage)
    }

    fun getEmbedding(bitmap: Bitmap): FloatArray {
        tImage = loadImage(bitmap)

        interpreter.run(tImage.buffer, tBuffer.buffer.rewind())

        return tBuffer.floatArray
    }

    private fun recognize(
        embedding: FloatArray,
        persons: ArrayList<Person>,
        isUsbCamera: Boolean
    ): String {
        if (persons.isNotEmpty()) {
            val similarities = ArrayList<Float>()
            persons.forEach {
                similarities.add(cosineSimilarity(it.embedding, embedding))
            }
            val maxVal = similarities.maxOrNull()!!

            val minimumSimilarAccuracy = if (isUsbCamera) {
                0.2
            } else {
                0.6
            }

            if (maxVal > minimumSimilarAccuracy) {
                val person = persons[similarities.indexOf(maxVal)]
                if (!detectedPersons.contains(person.name)) {
                    detectedPersons.add(person.name)
                    soundAlert.speakOut(person.name + " is detected.", TextToSpeech.QUEUE_ADD)
                }
                return "${person.name} ${
                    (maxVal * 100).toString().take(2)
                } %"
            }
        }
        return "unknown"
    }

    fun resetDetectedPersons() {
        detectedPersons.clear()
        print("Persons cleared")
    }

    private fun cosineSimilarity(A: FloatArray?, B: FloatArray?): Float {
        if (A == null || B == null || A.isEmpty() || B.isEmpty() || A.size != B.size) {
            return 2.0F
        }

        var sumProduct = 0.0
        var sumASq = 0.0
        var sumBSq = 0.0
        for (i in A.indices) {
            sumProduct += (A[i] * B[i]).toDouble()
            sumASq += (A[i] * A[i]).toDouble()
            sumBSq += (B[i] * B[i]).toDouble()
        }
        return if (sumASq == 0.0 && sumBSq == 0.0) {
            2.0F
        } else (sumProduct / (sqrt(sumASq) * sqrt(sumBSq))).toFloat()
    }

    fun detectFaces(
        cascadeClassifier: CascadeClassifier,
        mRGBA: Mat,
        persons: ArrayList<Person>,
        isUsbCamera: Boolean
    ) {
        var minimumFaceAccuracy: Int
        var minimumSizeFaceDetection: Size
        if (isUsbCamera) {
            minimumFaceAccuracy = -1
            minimumSizeFaceDetection = Size()
        } else {
            minimumFaceAccuracy = 2
            minimumSizeFaceDetection = Size(112.0, 112.0)
        }

        // Detect faces
        val detectedFaces = MatOfRect()
        cascadeClassifier.detectMultiScale(
            mRGBA,
            detectedFaces,
            1.1,
            minimumFaceAccuracy,
            2,
            minimumSizeFaceDetection,
            Size()
        )

        // Draw rectangle for detected faces
        for (rect: Rect in detectedFaces.toArray()) {
            val m = mRGBA.submat(rect)
            mBitmap = Bitmap.createBitmap(m.width(), m.height(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(m, mBitmap)
            val res = recognize(getEmbedding(mBitmap), persons, isUsbCamera)
            val scalar = if (res == "unknown") {
                Scalar(255.0, 0.0, 0.0)
            } else Scalar(0.0, 255.0, 0.0)

            Imgproc.rectangle(
                mRGBA,
                Point(rect.x.toDouble(), rect.y.toDouble()),
                Point(rect.x.toDouble() + rect.width, rect.y.toDouble() + rect.height),
                scalar, 1
            )

            Imgproc.putText(
                mRGBA,
                res,
                Point(rect.x.toDouble(), rect.y.toDouble() - 5.0),
                2,
                1.0,
                scalar, 2
            )
        }
    }
}